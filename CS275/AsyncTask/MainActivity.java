package com.example.jhibbele.asynctask;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public final static String TAG = "AsyncTask";
    private Button mButton;
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButton = (Button) findViewById(R.id.button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "onClick");
                new DownloaderTask().execute();
            }
        });

        mTextView = (TextView) findViewById(R.id.textView);
        mTextView.setText("hello");
    }

    private void updateTextViewWithHTML(String htmlmsg) {
        mTextView.setText(Html.fromHtml(htmlmsg));
    }

    private class DownloaderTask extends AsyncTask<Void, Void, Info> {
        @Override
        protected Info doInBackground(Void... params) {
            Log.i(TAG, "hello from background task");
            return new Fetcher().fetchItems();
         }

        @Override
        protected void onPostExecute(Info i) {
            Log.i(TAG, "onPostExecute called with " + i.toString());
            updateTextViewWithHTML(i.toString());
        }
    }
}
