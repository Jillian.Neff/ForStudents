#include <stdio.h>
#include <stdlib.h>
#include "pqueue.jhibbele.h"

#define NUM_THINGS 10

typedef enum EventTypeEnum {
  THING_SUBMITTED,
  THING_STARTS,
  THING_ENDS
} EventType;

typedef struct {
  int id;
  char name[32];
  int duration;
  int startWait;
} Thing;

typedef struct {
  EventType eventType;
  Thing *thing;
} Event;

void printEvent(void *data) {
  Thing *thing;
  Event *event;
  event = (Event *) data;
  thing = event->thing;
  if (event->eventType == THING_SUBMITTED)
    printf("[Thing submitted (id=%d name=%s duration=%d)]\n",
           thing->id, thing->name, thing->duration);
  else if (event->eventType == THING_STARTS)
    printf("[start Thing (id=%d name=%s duration=%d)]\n",
           thing->id, thing->name, thing->duration);
  else if (event->eventType == THING_ENDS)
    printf("[end Thing (id=%d name=%s duration=%d)]\n",
           thing->id, thing->name, thing->duration);
}


int main(int argc, char *argv[]) {
  long seed;
  Thing *thing;
  int numThings;
  Event *event, *newEvent;
  PQueueNode *eventQueue;
  PQueueNode *thingQueue;
  int i, startTime;
  int currentTime, thingMachineIsBusy;
  int totalWaitTime;
  int delta, waitTime;
  double d;

//time(&seed);
//srand48(seed);

  eventQueue = NULL;
  thingQueue = NULL;

  startTime = 0;
  thingMachineIsBusy = 0;
  totalWaitTime = 0;

  numThings = NUM_THINGS;

  for (i=1; i<=numThings; ++i) {
    // create a thing
    thing = (Thing *) malloc(sizeof(Thing));
    thing->id = i;
    sprintf(thing->name, "thing-%d", i);
    thing->startWait = 0;

    // assign a duration for this event: random integer in the
    // range [5, 15)
    d = drand48();
    thing->duration = 5 + (int) (10.0 * d);

    // assign a start time for this event: random integer in the range
    // [1, 10)
    d = drand48();
    delta = (int) 1 + (int) (9.0 * d);
    startTime = startTime + delta;

    // create an event for this thing
    event = (Event *) malloc(sizeof(Event));
    event->eventType = THING_SUBMITTED;
    event->thing = thing;
    enqueue(&eventQueue, startTime, event);
  }

  printf("here is the event queue after creating %d things\n", numThings);
  printQueue(eventQueue, printEvent);
  printf("\n");

  currentTime = getMinPriority(eventQueue);
  event = dequeue(&eventQueue);
  while (event != NULL) {
    thing = event->thing;
    if (event->eventType == THING_SUBMITTED) {
      thing->startWait = currentTime;
      if (thingMachineIsBusy == 0) {
        // create an event at currentTime to start this thing
        newEvent = (Event *) malloc(sizeof(Event));
        newEvent->eventType = THING_STARTS;
        newEvent->thing = thing;
        enqueue(&eventQueue, currentTime, newEvent);
        thingMachineIsBusy = 1;
      } else {
        // can't start: put this thing in the thing queue
        printf("t = %d: %s wants to start but must go into the thingQueue\n", currentTime, thing->name);
        enqueue(&thingQueue, 0, thing);
      }
    } else if (event->eventType == THING_STARTS) {
//      printf("t = %d: %s starts\n", currentTime, thing->name);
        waitTime = currentTime - thing->startWait;
        printf("t = %d: %s starts; wait time = %d\n",
               currentTime, thing->name, waitTime);
        totalWaitTime += waitTime;
        // create an event in the future for the termination of this thing
        newEvent = (Event *) malloc(sizeof(Event));
        newEvent->eventType = THING_ENDS;
        newEvent->thing = thing;
        enqueue(&eventQueue, currentTime + thing->duration, newEvent);
    } else if (event->eventType == THING_ENDS) {
      thing = event->thing;
      printf("t = %d: %s ends\n", currentTime, thing->name);
      // see if there is a thing in the thingQueue
      if (queueLength(thingQueue) > 0) {
        thing = dequeue(&thingQueue);
//      printf("t = %d: %s starts; wait time = %d\n",
//             currentTime, thing->name, waitTime);
        // create an event in the future for the termination of this thing
        newEvent = (Event *) malloc(sizeof(Event));
        newEvent->eventType = THING_STARTS;
        newEvent->thing = thing;
        enqueue(&eventQueue, currentTime, newEvent);
      } else {
        // current thing finished, so thing machine is not busy
        thingMachineIsBusy = 0;
      }
    }

    currentTime = getMinPriority(eventQueue);
    if (currentTime >= 0)
      printf("currentTime = %d\n", currentTime);
    printQueue(eventQueue, printEvent);
    printf("\n");

    event = dequeue(&eventQueue);
  }

  printf("\n");
  printf("%d things; mean wait time = %.2f\n", numThings,
         (double) totalWaitTime / numThings);
  return(0);
}
