#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUFLEN 256

char *infile = "addresses.txt";

int main(int argc, char **argv) {
  char buffer[BUFLEN];
  FILE *fp;
  char *chp;
  int val;
  int numRead;

  fp = fopen(infile, "r");
  if (fp == NULL) {
    fprintf(stderr, "cannot read file '%s'\n", argv[1]);
    return(8);
  }

  numRead = 0;

  chp = fgets(buffer, BUFLEN, fp);
  while ( numRead < 16 && chp != NULL ) {
    buffer[strlen(buffer)-1] = '\0';
    val = atoi(buffer);
    printf("val #%d is %d\n", numRead, val);
    chp = fgets(buffer, BUFLEN, fp);
    ++numRead;
  }

  fclose(fp);

  return(0);
}
