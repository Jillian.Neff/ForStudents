// to use this, you must have these includes:
// #include <math.h>
// #include <stdlib.h>
// this code will work on macOS or Linux

int genExpRand(double mean) {
  double r, t;
  int rtnval;
  r = drand48();
  t = -log(1-r) * mean;
  rtnval = (int) floor(t);
  if (rtnval == 0)
    rtnval = 1;
  return(rtnval);
}
