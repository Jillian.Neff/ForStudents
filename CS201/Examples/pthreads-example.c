#include <pthread.h>
#include <stdio.h>

#define NUMCHILDREN 2

void *runner(void *param);

typedef struct {
  int lowVal;
  int highVal;
  int sum;
} SumStruct;

int main(int argc, char *argv[]) {
  SumStruct data[NUMCHILDREN];   // holds data we want to give to child thread
  pthread_t tid[NUMCHILDREN];    // thread identifier
  pthread_attr_t attr; // thread attributes
  int bigSum;
  int i;

  data[0].lowVal = 1;
  data[0].highVal = 50;

  data[1].lowVal = 51;
  data[1].highVal = 100;

  // get default thread attributes
  pthread_attr_init(&attr);

  for (i=0; i<NUMCHILDREN; ++i) {
    // create child thread
    pthread_create(&tid[i], &attr, runner, &data[i]);
  }

  for (i=0; i<NUMCHILDREN; ++i) {
    // wait for the child threads to terminate
    pthread_join(tid[i], NULL);
  }

  bigSum = 0;
  for (i=0; i<NUMCHILDREN; ++i) {
    bigSum = bigSum + data[i].sum;
  }

  printf("sum = %d\n", bigSum);
  return(0);
}

void *runner(void *param) {
  SumStruct *data;
  int i, sum;

  data = (SumStruct *) param;

  printf("(R) I am runner; will sum integers from %d to %d\n",
          data->lowVal, data->highVal);

  sum = 0;
  for (i=data->lowVal; i<=data->highVal; ++i)
    sum = sum + i;

  data->sum = sum;

  printf("(R) sum is %d\n", data->sum);

  pthread_exit(0);
}
