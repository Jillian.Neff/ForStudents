#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <semaphore.h>
//#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>

#define BUFFER_SIZE 32
#define SEM_1_NAME "/SEM1"
#define SEM_2_NAME "/SEM2"

int main(int argc, char *argv[]) {
  int pid, done;
  int memid;
  int i, j;
  int key = 35;
  int rtnval;
  char *ptr;
  char buffer[BUFFER_SIZE];
  sem_t *sem1, *sem2;

  sem1 = sem_open(SEM_1_NAME, O_CREAT, 0666, 0);
  if (sem1 == NULL) {
    fprintf(stderr, "sem_open() failed\n");
    return(8);
  }
  sem2 = sem_open(SEM_2_NAME, O_CREAT, 0666, 0);
  if (sem2 == NULL) {
    fprintf(stderr, "sem_open() failed\n");
    return(8);
  }

  pid = fork();
  if (pid < 0) {
    printf("fork failed\n");
    return(8);
  }

  if (pid > 0) {
    // this is the parent
    printf("I am the parent, and my pid is %d\n", getpid());
    sem_post(sem1);
    sem_wait(sem2);
    wait(NULL);
  }  else {
    // this is the child
    pid = getpid();
    printf("I am the child, and my pid is %d\n", pid);
    sem_wait(sem1);
    sem_post(sem2);
    return(0);
  }

  return(0);
}
