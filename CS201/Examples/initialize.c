void initializeOne(int **A1) {
  A1[0][0] = 6; A1[0][1] = 2; A1[0][2] = 4; 
  A1[0][3] = 5; A1[0][4] = 3; A1[0][5] = 9; 
  A1[0][6] = 1; A1[0][7] = 8; A1[0][8] = 7; 
  A1[1][0] = 5; A1[1][1] = 1; A1[1][2] = 9; 
  A1[1][3] = 7; A1[1][4] = 2; A1[1][5] = 8; 
  A1[1][6] = 6; A1[1][7] = 3; A1[1][8] = 4; 
  A1[2][0] = 8; A1[2][1] = 3; A1[2][2] = 7; 
  A1[2][3] = 6; A1[2][4] = 1; A1[2][5] = 4; 
  A1[2][6] = 2; A1[2][7] = 9; A1[2][8] = 5; 
  A1[3][0] = 1; A1[3][1] = 4; A1[3][2] = 3; 
  A1[3][3] = 8; A1[3][4] = 6; A1[3][5] = 5; 
  A1[3][6] = 7; A1[3][7] = 2; A1[3][8] = 9; 
  A1[4][0] = 9; A1[4][1] = 5; A1[4][2] = 8; 
  A1[4][3] = 2; A1[4][4] = 4; A1[4][5] = 7; 
  A1[4][6] = 3; A1[4][7] = 6; A1[4][8] = 1; 
  A1[5][0] = 7; A1[5][1] = 6; A1[5][2] = 2; 
  A1[5][3] = 3; A1[5][4] = 9; A1[5][5] = 1; 
  A1[5][6] = 4; A1[5][7] = 5; A1[5][8] = 8; 
  A1[6][0] = 3; A1[6][1] = 7; A1[6][2] = 1; 
  A1[6][3] = 9; A1[6][4] = 5; A1[6][5] = 6; 
  A1[6][6] = 8; A1[6][7] = 4; A1[6][8] = 2; 
  A1[7][0] = 4; A1[7][1] = 9; A1[7][2] = 6; 
  A1[7][3] = 1; A1[7][4] = 8; A1[7][5] = 2; 
  A1[7][6] = 5; A1[7][7] = 7; A1[7][8] = 3; 
  A1[8][0] = 2; A1[8][1] = 8; A1[8][2] = 5; 
  A1[8][3] = 4; A1[8][4] = 7; A1[8][5] = 3; 
  A1[8][6] = 9; A1[8][7] = 1; A1[8][8] = 6; 
}

void initializeTwo(int **A2) {
  A2[0][0] = 6; A2[0][1] = 2; A2[0][2] = 4; 
  A2[0][3] = 5; A2[0][4] = 3; A2[0][5] = 9; 
  A2[0][6] = 1; A2[0][7] = 8; A2[0][8] = 7; 
  A2[1][0] = 5; A2[1][1] = 0; A2[1][2] = 9; 
  A2[1][3] = 7; A2[1][4] = 2; A2[1][5] = 8; 
  A2[1][6] = 6; A2[1][7] = 3; A2[1][8] = 4; 
  A2[2][0] = 8; A2[2][1] = 3; A2[2][2] = 7; 
  A2[2][3] = 6; A2[2][4] = 1; A2[2][5] = 4; 
  A2[2][6] = 2; A2[2][7] = 9; A2[2][8] = 5; 
  A2[3][0] = 1; A2[3][1] = 4; A2[3][2] = 3; 
  A2[3][3] = 8; A2[3][4] = 6; A2[3][5] = 5; 
  A2[3][6] = 7; A2[3][7] = 2; A2[3][8] = 9; 
  A2[4][0] = 9; A2[4][1] = 5; A2[4][2] = 8; 
  A2[4][3] = 2; A2[4][4] = 4; A2[4][5] = 7; 
  A2[4][6] = 3; A2[4][7] = 6; A2[4][8] = 1; 
  A2[5][0] = 7; A2[5][1] = 6; A2[5][2] = 2; 
  A2[5][3] = 3; A2[5][4] = 9; A2[5][5] = 1; 
  A2[5][6] = 4; A2[5][7] = 5; A2[5][8] = 8; 
  A2[6][0] = 3; A2[6][1] = 7; A2[6][2] = 1; 
  A2[6][3] = 9; A2[6][4] = 5; A2[6][5] = 6; 
  A2[6][6] = 8; A2[6][7] = 0; A2[6][8] = 2; 
  A2[7][0] = 4; A2[7][1] = 9; A2[7][2] = 6; 
  A2[7][3] = 1; A2[7][4] = 8; A2[7][5] = 2; 
  A2[7][6] = 5; A2[7][7] = 7; A2[7][8] = 3; 
  A2[8][0] = 2; A2[8][1] = 8; A2[8][2] = 5; 
  A2[8][3] = 4; A2[8][4] = 7; A2[8][5] = 3; 
  A2[8][6] = 9; A2[8][7] = 1; A2[8][8] = 6; 
}
