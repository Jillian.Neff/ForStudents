// reader thread and writer thread accessing a shared buffer,
// using a mutex to synchronize their access of a shared
// variable
//
// here's the writer:
// while (true) {
//   while (counter == BUFFER_SIZE)
//     /* do nothing */ ;
//
//   buffer[in] = next_item_produced;
//   in = (in + 1) % BUFFER_SIZE;
//   lock();
//   ++counter;
//   unlock();
// }
//
// here's the reader:
// while (true) {
//   while (counter == 0)
//     /* do nothing */ ;
//
//   next_item_consumed = buffer[out];
//   out = (out + 1) % BUFFER_SIZE;
//   lock();
//   --counter;
//   unlock();
// }

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>

#define BUFFER_SIZE 16

void *writer(void *);
void *reader(void *);

char buffer[BUFFER_SIZE];
int counter;
pthread_mutex_t lock;

int main() {
  counter = 0;

  pthread_t reader_tid, writer_tid;

  pthread_mutex_init(&lock, NULL);

  pthread_create(&writer_tid, NULL, writer, NULL);
  pthread_create(&reader_tid, NULL, reader, NULL);

  pthread_join(writer_tid, NULL);
  pthread_join(reader_tid, NULL);

  return(0);
}

//----------------------------------------------------

void *writer(void *param) {
  char *message = "now is the time for all good men\nto come to the aid of their party\n";
  char next_item_produced;
  int i, len, in, done, space_is_available;

  len = strlen(message);
  in = 0;

  // send the NULL character at the end of the string as well
  for (i=0; i<=len; ++i) {
    next_item_produced = message[i];

    space_is_available = 0;
    while ( ! space_is_available ) {
      pthread_mutex_lock(&lock);
      if (counter < BUFFER_SIZE)
        space_is_available = 1;
      pthread_mutex_unlock(&lock);
    }

    buffer[in] = next_item_produced;
    in = (in + 1) % BUFFER_SIZE;
    pthread_mutex_lock(&lock);
    ++counter;
    pthread_mutex_unlock(&lock);
  }

  pthread_exit(0);
}

//----------------------------------------------------

void *reader(void *param) {
  int out, done, item_is_available;
  char next_item_consumed;

  out = 0;
  done = 0;

  while (! done) {
    item_is_available = 0;
    while ( ! item_is_available ) {
      pthread_mutex_lock(&lock);
      if (counter > 0)
        item_is_available = 1;
      pthread_mutex_unlock(&lock);
    }

    next_item_consumed = buffer[out];
    if (next_item_consumed == 0)
      done = 1;
    else {
      printf("%c", next_item_consumed);
      out = (out + 1) % BUFFER_SIZE;
      pthread_mutex_lock(&lock);
      --counter;
      pthread_mutex_unlock(&lock);
    }
  }

  pthread_exit(0);
}
