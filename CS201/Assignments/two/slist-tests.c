#include <stdio.h>
#include <string.h>
#include "slist.jhibbele.h"

main() {
  StateListNode *theList = NULL;
  char name[MAX_STATE_NAME_LENGTH];
  int population;
  int rc;
 
  strcpy(name, "Kentucky"); 
  rc = insertState(&theList, name, 4468402);
  if (rc == 0)
    printf("inserted %s\n", name);
  else
    printf("failed to insert %s\n", name);
 
  strcpy(name, "Vermont"); 
  rc = insertState(&theList, name, 626299);
  if (rc == 0)
    printf("inserted %s\n", name);
  else
    printf("failed to insert %s\n", name);
 
  strcpy(name, "Iowa"); 
  rc = insertState(&theList, name, 3156145);
  if (rc == 0)
    printf("inserted %s\n", name);
  else
    printf("failed to insert %s\n", name);
 
  strcpy(name, "New Mexico"); 
  rc = insertState(&theList, name, 2095428);
  if (rc == 0)
    printf("inserted %s\n", name);
  else
    printf("failed to insert %s\n", name);
 
  strcpy(name, "Delaware"); 
  rc = insertState(&theList, name, 967171);
  if (rc == 0)
    printf("inserted %s\n", name);
  else
    printf("failed to insert %s\n", name);
 
  strcpy(name, "Vermont"); 
  rc = insertState(&theList, name, 626299);
  if (rc == 0)
    printf("inserted %s\n", name);
  else
    printf("failed to insert %s\n", name);
 
  strcpy(name, "Utah"); 
  rc = insertState(&theList, name, 3161105);
  if (rc == 0)
    printf("inserted %s\n", name);
  else
    printf("failed to insert %s\n", name);

  printList(theList);

  //---------------------------------------------------------------

  strcpy(name, "Utah");
  rc = findState(theList, name, &population);
  if (rc == 0)
    printf("found %s: population = %d\n", name, population);
  else
    printf("did not find state '%s'\n", name);

  strcpy(name, "Kansas");
  rc = findState(theList, name, &population);
  if (rc == 0)
    printf("found %s: population = %d\n", name, population);
  else
    printf("did not find state '%s'\n", name);

  strcpy(name, "Vermont");
  rc = findState(theList, name, &population);
  if (rc == 0)
    printf("found %s: population = %d\n", name, population);
  else
    printf("did not find state '%s'\n", name);

  strcpy(name, "Alabama");
  rc = findState(theList, name, &population);
  if (rc == 0)
    printf("found %s: population = %d\n", name, population);
  else
    printf("did not find state '%s'\n", name);

  strcpy(name, "New Mexico");
  rc = findState(theList, name, &population);
  if (rc == 0)
    printf("found %s: population = %d\n", name, population);
  else
    printf("did not find state '%s'\n", name);

  //---------------------------------------------------------------

  strcpy(name, "Utah");
  rc = deleteState(&theList, name);
  if (rc == 0)
    printf("deleted '%s'\n", name);
  else
    printf("failed to delete '%s'\n", name);

  strcpy(name, "Vermont");
  rc = deleteState(&theList, name);
  if (rc == 0)
    printf("deleted '%s'\n", name);
  else
    printf("failed to delete '%s'\n", name);

  strcpy(name, "Nevada");
  rc = deleteState(&theList, name);
  if (rc == 0)
    printf("deleted '%s'\n", name);
  else
    printf("failed to delete '%s'\n", name);

  strcpy(name, "Hawaii"); 
  rc = insertState(&theList, name, 1420491);
  if (rc == 0)
    printf("inserted %s\n", name);
  else
    printf("failed to insert %s\n", name);

  strcpy(name, "Utah");
  rc = deleteState(&theList, name);
  if (rc == 0)
    printf("deleted '%s'\n", name);
  else
    printf("failed to delete '%s'\n", name);

  strcpy(name, "Iowa");
  rc = findState(theList, name, &population);
  if (rc == 0)
    printf("found %s: population = %d\n", name, population);
  else
    printf("did not find state '%s'\n", name);

  strcpy(name, "New Mexico");
  rc = deleteState(&theList, name);
  if (rc == 0)
    printf("deleted '%s'\n", name);
  else
    printf("failed to delete '%s'\n", name);

  strcpy(name, "Delaware");
  rc = deleteState(&theList, name);
  if (rc == 0)
    printf("deleted '%s'\n", name);
  else
    printf("failed to delete '%s'\n", name);

  strcpy(name, "Kentucky");
  rc = deleteState(&theList, name);
  if (rc == 0)
    printf("deleted '%s'\n", name);
  else
    printf("failed to delete '%s'\n", name);

  strcpy(name, "Hawaii");
  rc = deleteState(&theList, name);
  if (rc == 0)
    printf("deleted '%s'\n", name);
  else
    printf("failed to delete '%s'\n", name);

  strcpy(name, "Iowa");
  rc = deleteState(&theList, name);
  if (rc == 0)
    printf("deleted '%s'\n", name);
  else
    printf("failed to delete '%s'\n", name);

  printList(theList);

  strcpy(name, "Vermont");
  rc = findState(theList, name, &population);
  if (rc == 0)
    printf("found %s: population = %d\n", name, population);
  else
    printf("did not find state '%s'\n", name);
}
