#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "warmup.netid.h" // change this to be your netid

#define TRUE 1
#define FALSE 0

//--------------------------------------------------------------------

int testHarnessOne(const char *fcnname, int n1, int n2, int expsum, int expdiff, int exprtnval) {
  int rtnval, sum, diff;
  int errflag = 0;

  rtnval = addSubtract(n1, n2, &sum, &diff);
  if (sum != expsum) {
    printf("error in %s: got sum = %d; expect %d\n", fcnname, sum, expsum);
    errflag = 1;
  }

  if (diff != expdiff) {
    printf("error in %s: got diff = %d; expect %d\n", fcnname, diff, expdiff);
    errflag = 1;
  }

  if (rtnval != exprtnval) {
    printf("error in %s: got rtnval = %d; expect %d\n", fcnname, rtnval, exprtnval);
    errflag = 1;
  }

  return(errflag);
}

//-----------------------------------------------------------------------------

int testHarnessTwo(const char *fcnname, int id, char *name, StudentData **rec, int expnull, int exprtnval) {
  int rtnval;
  int errflag;
  StudentData *localrec;

  errflag = 0;

  rtnval = createRecord(id, name, rec);
  if (rec == NULL)
    if ( ! expnull) {
      printf("error in %s: NULL pointer for rec; expected non-NULL\n", fcnname);
      errflag = 1;
  } else {
    if (rec != NULL && expnull) {
      printf("error in %s: non-NULL pointer for rec; expected NULL\n", fcnname);
      errflag = 1;
    } else {
      if (*rec == NULL) {
        printf("error in %s: NULL pointer for *rec; expected non-NULL\n", fcnname);
        errflag = 1;
      } else {
        localrec = *rec;
        if (localrec->id != id) {
          printf("error in %s: id for *rec is %d; expected %d\n", fcnname, localrec->id, id);
          errflag = 1;
        }
        if (strcmp(localrec->name, name)) {
          printf("error in %s: name for *rec is '%s'; expected '%s'\n", fcnname, localrec->name, name);
          errflag = 1;
        }
      }
    }
  }

  if (rtnval != exprtnval) {
    printf("error in %s: rtnval = %d; expected %d\n", fcnname, rtnval, exprtnval);
    errflag = 1;
  }

  return(errflag);
}

//-----------------------------------------------------------------------------

int testOne() {
  int n1, n2;
  int sum, diff, rtnval;

  n1 = 5;
  n2 = 6;
  rtnval = testHarnessOne(__FUNCTION__, n1, n2, n1+n2, n1-n2, -1);
  return(rtnval);
}

//-----------------------------------------------------------------------------

int testTwo() {
  int n1, n2;
  int sum, diff, rtnval;

  n1 = 6;
  n2 = 6;
  rtnval = testHarnessOne(__FUNCTION__, n1, n2, n1+n2, n1-n2, 0);
  return(rtnval);
}

//-----------------------------------------------------------------------------

int testThree() {
  int n1, n2;
  int sum, diff, rtnval;

  n1 = 6;
  n2 = 5;
  rtnval = testHarnessOne(__FUNCTION__, n1, n2, n1+n2, n1-n2, 1);
  return(rtnval);
}

//-----------------------------------------------------------------------------

int testFour() {
  int id, rtnval;
  char *name = "Thomas Smith";
  StudentData *rec;

  id = 45;
  rtnval = testHarnessTwo(__FUNCTION__, id, name, &rec, FALSE, 0);
  return(rtnval);
}

//-----------------------------------------------------------------------------

int testFive() {
  int id, rtnval;
  char *name = "Thomas Smith very very very long name";
  StudentData *rec;

  id = 45;
  rtnval = testHarnessTwo(__FUNCTION__, id, name, &rec, TRUE, 1);
  return(rtnval);
}

//-----------------------------------------------------------------------------

int testSix() {
  int id, rtnval;
  char *name = "Thomas Smith";
  StudentData *rec;

  id = -5;
  rtnval = testHarnessTwo(__FUNCTION__, id, name, &rec, TRUE, 1);
  return(rtnval);
}

//-----------------------------------------------------------------------------

main() {
  int numFails = 0;
  int rtnval;

  printf("\n");

  rtnval = testOne();
  if (rtnval != 0)
    ++numFails;
  rtnval = testTwo();
  if (rtnval != 0)
    ++numFails;
  rtnval = testThree();
  if (rtnval != 0)
    ++numFails;

  rtnval = testFour();
  if (rtnval != 0)
    ++numFails;
  rtnval = testFive();
  if (rtnval != 0)
    ++numFails;
  rtnval = testSix();
  if (rtnval != 0)
    ++numFails;

  if (numFails == 0)
    printf("OK: all tests passed\n");
  else
    printf("%d tests failed\n", numFails);
}
