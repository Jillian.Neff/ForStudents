// here's what to do:
// * add a field to Pawn saying whether it's human or computer
// * if it's human, proceed as is
// * else call computer.move(pawn) [or something like that]

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.image.Image;
import javafx.application.Platform;
import java.util.HashMap;
import java.util.ArrayList;

public class GameDemoController {
  public enum ActionType {WAITING_FOR_BUTTON, WAITING_FOR_PAWN, WAITING_FOR_TILE, NONE};
  private int numPlayers = 2;
  private ArrayList<Image> images;
  private HashMap<Image, ImageView> imageViewMap = new HashMap<> ();
  private Board board;
  private int pawnCurrentTurn = -1;
  private ActionType currentAction = ActionType.NONE;

  @FXML
  private ImageView imageViewDisplayOne;

  @FXML
  private ImageView imageViewDisplayTwo;

  @FXML
  private ImageView imageViewFocus;

  @FXML
  private TextField statusTextField;

  @FXML
  private Button moveButton;

  @FXML
  private Button newGameButton;
  
  @FXML
  private Button endGameButton;

  public GameDemoController() {
    System.out.println("hello from GameDemoController()");
  }

  @FXML
  public void initialize() {
    board = Board.get();
    board.Initialize();
    images = new ArrayList<> ();

    imageViewFocus.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent> () {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("Tile pressed " + event.getX() + " " + event.getY());
        event.consume();
        if (currentAction == ActionType.WAITING_FOR_PAWN) {
          int tileNumber = convertClickToTile(event.getX(), event.getY());
          String desiredColor = board.getPawn(pawnCurrentTurn).getColor();
          System.out.println("you clicked on tile " + tileNumber);
          if ( board.getPawn(pawnCurrentTurn).getPosition() == tileNumber ) {
            currentAction = ActionType.WAITING_FOR_TILE;
            setStatusText("pick destination tile for " + desiredColor);
          } else {
            setStatusText("must pick a " + desiredColor + " pawn");
          }
        } else if (currentAction == ActionType.WAITING_FOR_TILE) {
          int tileNumber = convertClickToTile(event.getX(), event.getY());
          if ( board.isSpaceFree(tileNumber)) {
            board.movePawnTo(pawnCurrentTurn, tileNumber);
            drawImage(images.get(pawnCurrentTurn), tileNumber);
            currentAction = ActionType.WAITING_FOR_BUTTON;
            pawnCurrentTurn = (pawnCurrentTurn + 1) % numPlayers;
            String desiredColor = board.getPawn(pawnCurrentTurn).getColor();
            setStatusText("It's " + desiredColor + "'s turn");
          } else {
            setStatusText("pick an unoccupied space for pawn");
          }
        }
      }
    });

    Image image = new Image("pawn-red.png", 50, 50, false, false);
    if (image == null) {
      System.out.println("error getting pawn-red.png");
    }
    images.add(image);
    imageViewMap.put(image, imageViewDisplayOne);

    image = new Image("pawn-green.png", 50, 50, false, false);
    if (image == null) {
      System.out.println("error getting pawn-green.png");
    }

    images.add(image);
    imageViewMap.put(image, imageViewDisplayTwo);
    
    board.addPawn(0, "Red");
    board.addPawn(1, "Green");
  }

  @FXML
  void moveButtonListener(ActionEvent event) {
    System.out.println("hello from moveButton");
    if (currentAction == ActionType.WAITING_FOR_BUTTON) {
      currentAction = ActionType.WAITING_FOR_PAWN;
      String desiredColor = board.getPawn(pawnCurrentTurn).getColor();
      setStatusText("Pick a " + desiredColor + " token to move");
    }
  }

  @FXML
  void newGameButtonListener(ActionEvent event) {
    drawImage(images.get(0), 1);
    drawImage(images.get(1), 13);
    board.movePawnTo(0, 1);
    board.movePawnTo(1, 13);
    pawnCurrentTurn = 0;
    String desiredColor = board.getPawn(pawnCurrentTurn).getColor();
    setStatusText("It's " + desiredColor + "'s turn");
    currentAction = ActionType.WAITING_FOR_BUTTON;
  }
  
  @FXML
  void endGameButtonListener(ActionEvent event) {
    Platform.exit();
  }

  public void drawImage(Image image, int tileNumber) {
    ImageView imageView = imageViewMap.get(image);
    if (imageView != null) {
      int x = 0, y = 0;

      if (tileNumber <= 7) {
        x = 25 + 50*(tileNumber-1);
        y = 25;
      } else if (tileNumber >= 13 && tileNumber <= 19) {
        x = 25 + 50*(19-tileNumber);
        y = 325;
      } else if (tileNumber >= 8 && tileNumber <= 12) {
        x = 325;
        y = 25 + 50*(tileNumber-7);
      } else if (tileNumber >= 20 && tileNumber <= 24) {
        x = 24;
        y = 25 + 50*(25-tileNumber);
      }
      imageView.setX(x-25);
      imageView.setY(y-25);
      imageView.setFitHeight(50);
      imageView.setPreserveRatio(true);
      imageView.setImage(image);
    }
  }

  private int convertClickToTile(double x, double y) {
    int xpos, ypos;

    if (x < 50)
      xpos = 1;
    else if (x < 100)
      xpos = 2;
    else if (x < 150)
      xpos = 3;
    else if (x < 200)
      xpos = 4;
    else if (x < 250)
      xpos = 5;
    else if (x < 300)
      xpos = 6;
    else
      xpos = 7;

    if (y < 50)
      ypos = 1;
    else if (y < 100)
      ypos = 2;
    else if (y < 150)
      ypos = 3;
    else if (y < 200)
      ypos = 4;
    else if (y < 250)
      ypos = 5;
    else if (y < 300)
      ypos = 6;
    else
      ypos = 7;

    if (ypos == 1)
      return(xpos);

    if (ypos == 7)
      return(12+8-xpos);

    if (xpos == 7)
      return(6+ypos);

    if (xpos == 1)
      return(18+8-ypos);

    return(0);
  }

  void setStatusText(String text) {
    this.statusTextField.setText(text);
  }

}
