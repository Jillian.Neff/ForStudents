import book
import patron

class Checkout:
  def __init__(self, patron, book):
    self.patron = patron
    self.book = book

  def get_book(self):
    return self.book

  def get_patron(self):
    return self.patron
