class Patron:
  s_next_card_number = 0

  def __init__(self, name):
    self.name = name
    Patron.s_next_card_number = Patron.s_next_card_number + 1
    self.card_number = Patron.s_next_card_number
    self.checkouts = set()

  def to_string(self):
    s = self.name + ' id=' + str(self.card_number) + '; #checkout(s) = ' + \
          str(len(self.checkouts))
    return s

  def get_card_number(self):
    return self.card_number;

  def __eq__(self, other):
    return self.card_number == other.card_number

  def __hash__(self):
    return hash((self.name, self.card_number))

  def do_checkout(self, book):
    self.checkouts.add(book)

  def get_checkouts(self):
    return list(self.checkouts)
