import checkout

class Library:
  s_library = None

  @classmethod
  def get(self):
    if self.s_library is None:
      self.s_library = Library()
    return self.s_library

  def __init__(self):
    self.books = set()
    self.patrons = set()
    self.checkouts = set()

  def add_patron(self, patron):
    self.patrons.add(patron)

  def get_patrons(self):
    return self.patrons

  def add_book(self, book):
    self.books.add(book)

  def get_books():
    return self.books

  def find_patron(self, card_number):
    for p in self.patrons:
      if p.get_card_number() == card_number:
        return p
    return None

  def find_book(self, title):
    books = []
    for b in self.books:
      if b.get_title() == title:
        books.append(b)
    return books

  def do_checkout(self, p, b):
    if not self.is_checked_out(b):
      c = checkout.Checkout(p, b)
      self.checkouts.add(c)
      p.do_checkout(b)
      return c
    else:
      return None

  def is_checked_out(self, b):
    for c in self.checkouts:
      if c.get_book() == b:
        return True
    return False

  def show_checkouts(self):
    for c in self.checkouts:
      s = c.get_patron().to_string() + ' => ' + c.get_book().to_string()
      print(s)

  def get_checkouts(self, p):
    booklist = []
    for c in self.checkouts:
      if c.get_patron() == p:
        booklist.append(c.get_book())
    return booklist

  def return_book(self, p, b):
    for c in self.checkouts:
      if c.get_patron() == p and c.get_book() == b:
        self.checkouts.remove(c)
        return True
    return False
