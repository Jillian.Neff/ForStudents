import library
import simulation

#----------------------------------------

def main():
  lib = library.Library().get()

  sim = simulation.Simulation(lib)
  sim.run()

#----------------------------------------

main()
