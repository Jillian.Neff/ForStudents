class Book:
  def __init__(self, title, author, catalog_number):
    self.title = title
    self.author = author
    self.catalog_number = catalog_number

  def to_string(self):
    s = '"' + self.title + '" ' + self.author + ' ' + self.catalog_number
    return s

  def get_title(self):
    return self.title

  def __eq__(self, other):
    return self.title == other.title and self.author == other.author

  def __hash__(self):
    return hash((self.title, self.author))
