import book
import patron

class Simulation:
  def __init__(self, library):
    self.library = library

  def run(self):
    mary = patron.Patron('Mary')
    john = patron.Patron('John')
    self.library.add_patron(mary)
    self.library.add_patron(john)

    title_1 = 'Book One Title'
    title_2 = 'Book Two Title'
    book_1 = book.Book(title_1, 'Author Person 1', '800.1')
    book_2 = book.Book(title_2, 'Author Person 2', '800.2')
    self.library.add_book(book_1)
    self.library.add_book(book_2)

    p = self.library.find_patron(1)
    if p is not None:
      books = self.library.find_book(title_1)
      if len(books) > 0:
        print('check out ' + books[0].to_string() + ' to ' + p.to_string())
        self.library.do_checkout(p, books[0])
      else:
        print('cannot find book with title ' + title_1)

      books = self.library.find_book(title_2)
      if len(books) > 0:
        print('check out ' + books[0].to_string() + ' to ' + p.to_string())
        self.library.do_checkout(p, books[0])
      else:
        print('cannot find book with title ' + title_2)
    else:
      print('cannot find patron with card #1')

    print('here are the checkouts for the library:')
    self.library.show_checkouts()

    p = self.library.find_patron(1)
    if p is not None:
      books = self.library.get_checkouts(p)
      if len(books) > 0:
        print('return ' + books[0].to_string() + ' to ' + p.to_string())
    else:
      print('cannot find patron with card #1')

    print('here are the checkouts for the library')
    self.library.show_checkouts()
