import unittest
import library
import book
import patron

class TestCheckout(unittest.TestCase):
  library = None

  @classmethod
  def setUpClass(cls):
    # called one time, at beginning
    print('setUpClass()')
    cls.library = library.Library().get()
    title_1 = 'Book One Title'
    title_2 = 'Book Two Title'
    title_3 = 'Book Three Title'
    author_a = 'Author Person A'
    author_b = 'Author Person B'
    author_c = 'Author Person C'
    cls.book1 = book.Book(title_1, author_a, '800.1')
    cls.book2 = book.Book(title_2, author_b, '800.2')
    cls.book3 = book.Book(title_3, author_c, '800.3')
    cls.john = patron.Patron('John')
    cls.mary = patron.Patron('Mary')
    cls.library.add_patron(cls.john)
    cls.library.add_patron(cls.mary)
    cls.library.add_book(cls.book1)
    cls.library.add_book(cls.book2)
    cls.library.add_book(cls.book3)

  @classmethod
  def tearDownClass(cls):
    # called one time, at end
    print('tearDownClass()')
    
  def setUp(self):
    # called before every test
    print('setUp()')

  def tearDown(self):
    # called after every test
    print('tearDown()')

  #-------------------------------------------------------------
    
  def test_checkout_one(self):
    # check that the library shows that no books are checked out to john
    books = self.library.get_checkouts(self.john)
    self.assertEqual(len(books), 0)

    # check out a book to john
    c = self.library.do_checkout(self.john, self.book1)

    # check that the library shows one book checked out to john
    books = self.library.get_checkouts(self.john)
    self.assertEqual(len(books), 1)

    # check that the book checked out to john is book1
    if len(books) == 1:
      self.assertEqual(books[0], self.book1)

    # check that john shows one book checked out
    books = self.john.get_checkouts()
    self.assertEqual(len(books), 1)

    # check that the book checked out to john is book1
    if len(books) == 1:
      self.assertEqual(books[0], self.book1)
      
  #-------------------------------------------------------------
      
  def test_checkout_two(self):
    # check out a different book to mary
    c = self.library.do_checkout(self.mary, self.book2)
    self.assertIsNotNone(c)

    # check that the library shows one book checked out to mary
    books = self.library.get_checkouts(self.mary)
    self.assertEqual(len(books), 1)

    # check that the book checked out to mary is book2
    if len(books) == 1:
      self.assertEqual(books[0], self.book2)

    # check that mary shows one book checked out
    books = self.mary.get_checkouts()
    self.assertEqual(len(books), 1)

    # check that the book checked out to mary is book2
    if len(books) == 1:
      self.assertEqual(books[0], self.book2)

  #-------------------------------------------------------------
      
  def test_return(self):
    # return john's book--should return True
    rc = self.library.return_book(self.john, self.book1)
    self.assertTrue(rc)

    # try to return the same book again--should return False
    rc = self.library.return_book(self.john, self.book1)
    self.assertFalse(rc)

    # check that the library shows that john has no books checked out
    books = self.library.get_checkouts(self.john)
    self.assertEqual(len(books), 0)

    # check that john shows no books checked out
    # right now, this code is failing, because my code is incorrect--
    # I'm returning to the library but not to Patron
    books = self.john.get_checkouts()
    self.assertEqual(len(books), 0)
    
#-----------------------------------------

if __name__ == "__main__":
  unittest.main()
