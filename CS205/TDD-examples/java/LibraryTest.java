import org.junit.Assert;
//simport static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.After;
import org.junit.Test;
//import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matcher.*;

import java.util.List;
import java.util.ArrayList;


public class LibraryTest {
  private static Library library;
  private static Patron john;
  private static Patron mary;
  private static Book book1;
  private static Book book2;
  private static Book book3;
  
  @BeforeClass
  public static void setUp() {
    library = Library.get();
    john = new Patron("John");
    mary = new Patron("Mary");
    book1 = new Book("Book One Title", "Author Person 1", "800.1");
    book2 = new Book("Book Two Title", "Author Person 2", "800.2");
    book3 = new Book("Book Three Title", "Author Person 3", "800.3");
  }
  
  @After
  public void tearDown() {
    library.showCheckouts();
  }
  
  @Test
  public void testOne() {
    library.addPatron(john);
    library.addPatron(mary);

    library.addBook(book1);
    library.addBook(book2);
    
    List<Book> books = library.getCheckouts(john);
    Assert.assertEquals(books.size(), 0);
    
    // check a book out to john
    Checkout checkout = library.doCheckout(john, book1);
    Assert.assertNotNull(checkout);
    
    // check that the library shows one book checked out to john
    books = library.getCheckouts(john);
    Assert.assertEquals(books.size(), 1);
    
    // check that john shows one book checked out
    books = john.getCheckouts();
    Assert.assertEquals(books.size(), 1);
    
    // try to check the same book out to mary--should return null
    checkout = library.doCheckout(mary, book1);
    Assert.assertNull(checkout);
  }
  
  @Test
  public void testTwo() {
    // check a different book out to mary
    Checkout checkout = library.doCheckout(mary, book2);
    Assert.assertNotNull(checkout);
    
    // check that the library shows one book checked out to mary
    List<Book> books = library.getCheckouts(mary);
    Assert.assertEquals(books.size(), 1);
    
    // check that mary shows one book checked out
    books = mary.getCheckouts();
    Assert.assertEquals(books.size(), 1);
  }
  
  @Test
  public void testThree() {
    // return john's book--should return true
    System.out.println(john.toString());
    System.out.println(mary.toString());
    System.out.println("here it comes");
    boolean rc = library.returnBook(john, book1);
    Assert.assertTrue(rc);
    
    // try to return the same book again
    rc = library.returnBook(john, book1);
    Assert.assertFalse(rc);
    
    // check that the library shows John has no books checkout out
    List<Book> books = library.getCheckouts(john);
    Assert.assertEquals(books.size(), 0);
    
    // check that john shows no books checked out
    // right now, this test is failing, because my code is
    // incorrect--I'm returning to the library but not to Patron
    books = john.getCheckouts();
    Assert.assertEquals(books.size(), 0);
  }
  
  @Test
  public void testFour() {
    // try to a book that John hasn't checked out--should fail
    boolean rc = library.returnBook(john, book3);
    Assert.assertFalse(rc);
  }
  
  @Test
  public void testFive() {
    // try to return Mary's book to John--this should fail
    boolean rc = library.returnBook(john, book2);
    Assert.assertFalse(rc);
  }
}
