import java.util.List;
import java.util.ArrayList;

public class Library {
  private static Library sLibrary;
  
  private List<Book> books;
  private List<Patron> patrons;
  private List<Checkout> checkouts;
  
  private Library() {
    this.books = new ArrayList<Book> ();
    this.patrons = new ArrayList<Patron> ();
    this.checkouts = new ArrayList<Checkout> ();
  }
  
  public static Library get() {
    if (sLibrary == null)
      sLibrary = new Library();
    return sLibrary;
  }
  
  public void addPatron(Patron patron) {
    this.patrons.add(patron);
  }
  
  private List<Patron> getPatrons() {
    return this.patrons;
  }
  
  public void addBook(Book book) {
    this.books.add(book);
  }
  
  private List<Book> getBooks() {
    return this.books;
  }
  
  public Patron findPatron(int cardNumber) {
    for (Patron p : this.patrons) {
      if (p.getCardNumber() == cardNumber)
        return p;
    }
    return null;
  }
  
  public List<Book> findBook(String title) {
    List<Book> bookList = new ArrayList<Book> ();
    for (Book b : this.books) {
      if (b.getTitle() == title)
        bookList.add(b);
    }
    return bookList;
  }
  
  public Checkout doCheckout(Patron patron, Book book) {
    // first make sure book isn't already checkout out
    if ( ! isCheckedOut(book) ) {
      Checkout checkout = new Checkout(patron, book);
      this.checkouts.add(checkout);
      patron.doCheckout(book);
      return checkout;
    } else {
      return null;
    }
  }
  
  public boolean isCheckedOut(Book book) {
    for (Checkout c : this.checkouts) {
      if (c.getBook().equals(book))
        return true;
    }
    return false;
  }
  
  public void showCheckouts() {
    for (Checkout c : this.checkouts) {
      String s = c.getPatron().toString() + " => " + c.getBook().toString();
      System.out.println(s);
    }
  }
  
  public List<Book> getCheckouts(Patron p) {
    List<Book> bookList = new ArrayList<Book> ();
    for (Checkout c : this.checkouts) {
      if (c.getPatron().equals(p)) {
        bookList.add(c.getBook());
      }
    }
    return bookList;
  }
  
  public boolean returnBook(Patron patron, Book book) {
    for (Checkout c : this.checkouts) {
      if (c.getPatron().equals(patron) && c.getBook().equals(book)) {
        this.checkouts.remove(c);
        System.out.println("found book to return " + patron.toString());
        return true;
      }
    }
    
    System.out.println("did not find " + patron.toString() + " " + book.toString());
    return false;
  }
}